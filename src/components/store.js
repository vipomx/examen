import React, { Component } from 'react';
import logo from '../logo.svg';
import './store.css';
import Data from '../data.json';
import Cart from './cart';

class Store extends Component {
  render() {
    return (
      <div id="store" className="store-wrapper">
        <header className="store-header">
          <img src={logo} className="store-logo" alt="logo" />
          <h1>The Cart</h1>
        </header>
        <div className="store-container">
          {<Cart data={Data} />}
        </div>
      </div>
    );
  }
}


export default Store;