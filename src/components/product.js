import React, { PureComponent } from 'react';
import './product.css';


class Product extends PureComponent {
  state = {
    quantity: this.props.quantity,
    added: 'no-added'
  }

  addCart = (event) => {
    if (this.state.quantity !== 0) {
      this.setState({
        added: 'added'
      });
    }
    else {
      this.setState({
        added: 'added',
        quantity: 1
      });
    }
    
  }

  removeCart = (event) => {
    this.setState({
      added: 'no-added',
      quantity: 0
    });
  }

  increaseProduct = (event) => {
    if (this.state.quantity < this.props.availability) {
      this.setState({
        quantity: this.state.quantity + 1,
      });
    }
  }

  reduceProduct = (event) => {
    if (this.state.quantity >= 2) {
      this.setState({
        quantity: this.state.quantity - 1,
      });
    }
    else if (this.state.quantity === 1) {
      this.setState({
        added: 'no-added',
        quantity: 0
      });
    }
  }

  render() {
    return (
      <div className="product">
        <div className="p-image">
          <img
            src={ this.props.img }
            alt={ this.props.name }
            width={260}
            height={160}
            className="product-image"
          />
        </div>
        <div className="p-info">
          <div className="name">
            <h2>{ this.props.name }</h2>
          </div>
          <div className="price">
            <span>{ '$' + this.props.price }</span>
          </div>
        </div>
        <div className={ 'p-controls ' + this.state.added }>
          <div className="p-qcontrols">
            <button className="change-cart more" onClick={ this.increaseProduct }>+</button>
            <span className="p-number"> { this.state.quantity } </span>
            <button className="change-cart less" onClick={ this.reduceProduct }>-</button>
          </div>
          <button className="add-cart" onClick={ this.addCart }>Add to Cart</button>
          <button className="remove-cart" onClick={ this.removeCart }>Remove from Cart</button>
        </div>
      </div>
    );
  }
}


export default Product;