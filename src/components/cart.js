import React from 'react';
import Product from './product';


function Cart(data) {
  const prods = data['data']['data']['items'];
  return (
    <div className="product-list">
      {
        prods.map((item) => {
          return <Product {...item} key={item.id}/>;
        })
      }
    </div>
  )
}

export default Cart;